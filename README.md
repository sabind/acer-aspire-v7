Acer Aspire V7 on Debian
========================

This project tracks various tweaks I have found to improve the experience of using an Acer Aspire V7 laptop with Debian Linux.
The model I use is a V7-482PG-9617, for different models your experience may vary seeing as some of the tweaks are for hardware.

Currently these tweaks are intended for Debian 11 (Bullseye)

Debian installation and package repositories
--------------------------------------------

Debian is a great distribution, but unfortunately a lot of the drivers and firmware needed to get hardware to work have licenses which force Debian to qualify them as "non-free".

Debian does offer installation images which include "non-free" software, however I have not had a lot of luck installing from those images.

My preferred approach is to install from official Debian installation images and then add the "non-free" repositories after installation.
The caveat with this is that you need to plug a network cable into your laptop during installation since wireless hardware needs non-free firmware to work properly.

Once debian is installed you can edit `/etc/apt/sources.list` and ensure it mentions non-free.
Your sources line should look like this:
```
deb http://debian.mirror.rafal.ca/debian/ bullseye main contrib non-free
```

Your debian mirror will probably be different but the important thing is to include "contrib" and "non-free" after the word "main".

Once this is done you can run `sudo apt update` to download information about the available packages.
After that you should easily be able to follow the instructions in this guide since they frequently involve installing non-free packages.

Notes on Rebooting
------------------

Throughout this guide I will mention that something or other will work after rebooting.
I realize linux is great and you almost never need to reboot the whole OS if you restart the appropriate services.
That said, simply asking you to reboot keeps the guide a little shorter and less complex without getting into the details of which services you need to restart.

Wireless Networking
-------------------

The Acer Aspire comes with an Intel Wireless 7260, you can see this by running `lspci | grep -i network`.
This requires binary firmware from Intel available in the "firmware-iwlwifi" package.
One trick to find out which package you need is to do an apt search for the wireless model like this: `apt search 7260`.

The firmware package can be installed by running `apt install firmware-iwlwifi`.
On your next reboot, the wireless chip should work properly.

Graphics
--------

The Acer Aspire V7 comes with Nvidia Optimus which means it's able to switch between dedicated Nvidia graphics (for performance) and the integrated Intel graphics (for battery life and lower temperature, read less fan noise).
My V7-483PG comes with an Nvidia GeForce 750M and the integrated GPU from the Intel i7-4500U CPU.

The integrated GPU is referred to as the Haswell-ULT Integrated Graphics Controller and uses the i915 kernel driver which works out of the box with the drivers provided by the kernel.

For the Nvidia GPU I recommend using the proprietary drivers from Nvidia and this is where it gets tricky.
A lot of the information I'm using here I got from the great guide prepared by Debian volunteers at:
[Debian Wiki NVIDIA Optimus page](https://wiki.debian.org/NVIDIA%20Optimus)

After a lot of digging, experimenting and web searches I determined that the GeForce 750M unfortunately does not support the PRIME Render Offload described in the Debian Wiki page (the GPU may just be too old for it).
As for Bumblebee, I've used it in the past, but I feel like using Nvidia only offers better performance.

Below I will describe ways to configure the laptop to operate either with Intel only or Nvidia only.

There are many different generations of Nvidia drivers, determining the right version for the Aspire is done using the nvidia-detect tool.

First install the tool and run it:
```
sudo apt install nvidia-detect
nvidia-detect
```
This will report that you need to use the "nvidia-legacy-390xx-driver" (this may differ for different versions of the Acer Aspire).

Note: before starting this installation process, make sure you read the whole section and decide if you want to use Intel graphics or Nvidia. If you perform only some of the steps it may leave your graphics environment unusable.

NVIDIA Only Configuration (high performance)
--------------------------------------------

The driver can easily installed:
```
sudo apt install nvidia-legacy-390xx-driver
```

Nvidia normally doesn't need any additional configuration, however due to the switchable graphics, if you were to restart your display manager at this point, the X windows system would not start up properly.

In order for Xorg work and operate optimally in Nvidia only mode you'll need to do the following.

Download the [Nvidia only Xorg configuration file](xorg.conf.nvidia-only) and "install" it as follows:
`sudo cp xorg.conf.nvidia-only /etc/X11/xorg.conf`

Use xrandr to set the provider output source. Use the steps below for LightDM. I took these steps from the [Debian Wiki NVIDIA Optimus page](https://wiki.debian.org/NVIDIA%20Optimus) where they also describe how to do this for other display managers.

Create a display setup script:
```
sudo cat > /etc/lightdm/display_setup.sh << EOF
xrandr --setprovideroutputsource modesetting NVIDIA-0
xrandr --auto
xrandr --dpi 96
EOF
sudo chmod +x /etc/lightdm/display_setup.sh
```

Ensure that LightDM invokes this script:
```
sudo cat > /etc/lightdm/lightdm.conf << EOF
[SeatDefaults]
display-setup-script=/etc/lightdm/display_setup.sh
EOF
```

The Debian Wiki page tells you to testart LightDM at this point, however I would implement the next steps first and then do a reboot.

Enable modesetting for the nvidia kernel module:

```
sudo echo "options nvidia-drm modeset=1" >> /etc/modprobe.d/nvidia.conf
update-initramfs -u
```

Reboot the machine and LightDM should start properly.
Here are a couple of ways to verify that all went well.
Checking the Open GL renderer can be done by running:
`glxinfo -B`
This should display something like "OpenGL renderer string: GeForce GT 750M/PCIe/SSE2"

Check the performance by running 
`glxgears -info`
This will show some cool 3D gears hopefully getting rendered properly and running fast.
The terminal output should also indicate "GL_RENDERER   = GeForce GT 750M/PCIe/SSE2"

The third thing is to run `nvidia-settings`. This tool comes with the proprietary Nvidia drivers.
It provides plenty of information about the GPU. The added benefit is that it shows a big error message if the drivers are not loaded properly.

Intel Only Graphics Configuration (long battery life, lower temperatures, less fan noise)
-----------------------------------------------------------------------------------------

The easiest way to get the Acer Aspire to use only the integrated Intel GPU for the X windows system is to never install the proprietary Nvidia drivers mentioned in the previous section.

If you have installed the drivers and do not want to remove them you can use the [Intel only Xorg configuration file](xorg.conf.intel-only) which I have created.
This file can be "installed" by running the following:
`sudo cp xorg.conf.nvidia-only /etc/X11/xorg.conf`

If you keep your proprietary Nvidia drivers installed you can switch between the 2 configurations though it's not convenient.
Switching back to Intel after the configuration steps from the Nvidia only section will require you to undo some of the changes.
This will of course break the Nvidia configuration.

You will need to empty the display script used by LightDM as follows:
```
sudo echo "" > /etc/lightdm/display_setup.sh
```

You will need to switch the GLX provider to mesa by running:
```
sudo update-glx --config glx
```
In the menu you will need to select the "/usr/lib/mesa-diverted" option.

Touchpad
--------

The touchpad seems to work great in desktop environments like GNOME or Cinnamon (probably KDE, I have not tried).

That said, tap to click does not seem to work out of the box with window managers such as Openbox or display managers such as LightDM.
The touchpad included with the Aspire still works if pressed down to click... I guess that would be click to click. That behaviour is fairly unconfortable.

One way to fix this is to copy a [X11 libinput configuration file](40-libinput.conf) into /etc/X11/xorg.conf.d and restart LightDM using `systemctl restart lightdm`.
